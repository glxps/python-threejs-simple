# python-threejs-simple

Demonstrate in few code how to spin my experiments

> Light
> Object
> Particules
> Need Custom Shader


## how to run

```bash
python3 -m venv .env
source .env/bin/activate
pip install -r requirements.txt
```

## How to run the project
```bash
flask run
```
You can run the project with `FLASK_ENV=development` env variable with turn on the debuging mode.
```bash
FLASK_ENV=development flask run
```
Or more pernanentely exporting the variable
```bash
export FLASK_ENV=development
flask
```