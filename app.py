from flask import Flask
from flask import render_template, send_file

app = Flask(__name__)

@app.route("/<xpurl>")
def index(xpurl):
    if xpurl == 'favicon.ico':
        return send_file('./static/favicon.ico', mimetype='image/gif')
    else:
        return render_template(xpurl+'.html',page={'title':xpurl,'jsfile':xpurl+'.js','description':xpurl})